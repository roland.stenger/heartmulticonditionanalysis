import sys, os, glob
import numpy as np
import time
from sklearn.preprocessing import MinMaxScaler

#from keras.models import load_model

class BasePipeline:
    def __init__(self, **kwargs):
        """
        Init method for loading meta-parameters, defined in the file metadata.json
        """
        
        for key, value in kwargs.items(): 
            if key=='title':
                self.__title__ = value
            elif key=='qualifiedName':
                self.__qualifiedName__ = value 
            elif key=='version':
                self.__version__ = value
            elif key=='date':
                self.__date__ = value
            elif key=='path':
                self.__paths__ = value
                for k, v in value.items():
                    sys.path.append(v)
                    
        # make folders if not yet exist
        if not os.path.exists(kwargs['path']['dataProcessed'] + self.__qualifiedName__):
            os.makedirs(kwargs['path']['dataProcessed'] + self.__qualifiedName__)
        if not os.path.exists(kwargs['path']['savedModel'] + self.__qualifiedName__):
            os.makedirs(kwargs['path']['savedModel'] + self.__qualifiedName__)
        if not os.path.exists(kwargs['path']['test'] + self.__qualifiedName__):
            os.makedirs(kwargs['path']['test'] + self.__qualifiedName__)
        if not os.path.exists(kwargs['path']['notebook'] + self.__qualifiedName__):
            os.makedirs(kwargs['path']['notebook'] + self.__qualifiedName__)
    
    
    def fetchRawData(self):
        """
        Method for gathering the data from the folder, defined in metadata.json at path:gathering,
        super-class only defines the handler from gathering-folder as class-member
        """
        pass
    
    def fetchProcessedData(self):
        pass

    def gatherData(self):
        """
        Method for gathering the data from the folder, defined in metadata.json at path:gathering,
        super-class only defines the handler from gathering-folder as class-member
        """
        
        from data_gathering import _gatherH5Data, _gatherH5Coords, _gatherTDMSData
        
        self.gatherData = _gatherH5Data
        self.gatherCoords = _gatherH5Coords
        self.gatherTDMSData = _gatherTDMSData
        
        return
    
    def processData(self):
        """
        Method for processing the data from the folder, defined in metadata.json at path:processing,
        super-class only defines the handler from processing-folder as class-member
        """
    
        from data_transformer import CoordsSelector, DelayCoordinatesEmbedder
        from data_transformer import MinMaxScaler as OwnMinMaxScaler
        #from min_max_scaler import MinMaxScaler
        
        self.CoordsSelector = CoordsSelector
        self.DelayCoordinatesEmbedder = DelayCoordinatesEmbedder
        self.MinMaxScaler = MinMaxScaler
        self.OwnMinMaxScaler = OwnMinMaxScaler
        
        return
    
    def saveNumpy(self, *args, **kwargs):
        """
        Saves the arrays from the dict kwargs into the default folder, defined in __init__
        """
        
        folder = self.__paths__['dataProcessed'] + self.__qualifiedName__ + '/'
        
        if len(args) == 0:
            for key, value in kwargs.items(): 
                print ("save %s" %(key)) 
                np.save(folder + key, value)
            return
        else:
            for key, value in kwargs.items(): 
                if key in args:
                    print ("save %s" %(key)) 
                    np.save(folder + key, value)
                
    
    def loadNumpy(self, *args):
        """
        Loads the files in args from the default folder
        """
        
        folder = self.__paths__['dataProcessed'] + self.__qualifiedName__ + '/'
        data = {}
        
        if len(args) == 0:
            files = glob.glob(folder + '*.npy')
            for file in files:
                dict_name = os.path.splitext(os.path.basename(file))[0]
                d = {dict_name:np.load(file)}
                data = {**data, **d}
                
        #elif len(args) == 1:
        #    name = os.path.splitext(file)[0]
        #    d = {name:np.load(folder + args[0])}
        #    data = {**data, **d}

        else:
            for file in args:
                name = os.path.splitext(os.path.basename(file))[0]
                #name = os.path.splitext(file)[0]
                d = {name:np.load(folder + file + '.npy')}
                data = {**data, **d}

        return data
        
    def saveModel(self, model, **kwargs):
        folder_path = self.__paths__['savedModel'] + self.__qualifiedName__ + '/'
        folder_name = time.strftime("%Y_%m_%d-%H:%M:%S")
        
        os.makedirs(folder_path + folder_name)
        path = folder_path + folder_name
        
        name = 'model'
        for key, value in kwargs.items(): 
            if key=='name':
                name = value
                
        model.save(path + '/' + name + '.h5')
        
        print('save model')
        model_json = model.to_json()
        with open(path + name + '.json', "w") as json_file:
            json_file.write(model_json)
        
    def loadModel(self, **kwargs):
        path = self.__paths__['savedModel'] + self.__qualifiedName__ + '/'
        files = glob.glob(path + '*.h5')
        files_dates= [os.path.getmtime(f) for f in files]
        latest = [x for _,x in sorted(zip(files_dates,files))][-1]
        model = load_model(latest)
        
        return model
