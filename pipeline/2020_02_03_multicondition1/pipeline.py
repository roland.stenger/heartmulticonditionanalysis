import sys, json, os, glob
import numpy as np
import matplotlib.pyplot as plt

parentPath = os.path.abspath(os.path.join(__file__ ,"../.."))
sys.path.append(parentPath)

_path = os.path.abspath(os.path.join(__file__ ,".."))
sys.path.append(_path)
    
from basePipeline import BasePipeline

class ModelPipeline(BasePipeline):
    def __init__(self):
        with open(os.path.dirname(os.path.realpath(__file__)) + '/metadata.json', 'r') as f:
            kwargs = json.load(f)
        super().__init__(**kwargs)
        
    def fetchRawData(self, *args):
        """
        Parameters
        ----------
        *args : list of strings, codeword for wanted files
            possible arguents: 'ecg', 'tags'

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        
        super().fetchRawData()
        path = self.__paths__['dataRaw']
        
        data = {}
        for key in args:
            if key=='ecg':
                ecgs = []              
                files = glob.glob(path + 'electrode_signals/ecgs*')
                
                ecg_ids = []
                for file in files:
                    ecg_id = os.path.splitext(os.path.basename(file))[0][5:]
                    ecg_ids.append(ecg_id)
                    
                    ecg = np.load(file)
                    ecgs.append(ecg)
                ecgs = np.array(ecgs)
                
                data['ecg'] = ecgs
                data['ecg_ids'] =ecg_ids
            
            elif key=='positions':
                files = glob.glob(path + 'simulation_info/*.json')
                
                positions = [] 
                position_ids = []
                for file in files:
                    position_id = os.path.splitext(os.path.basename(file))[0]
                    position_ids.append(position_id)
                    with open(file, 'r') as f:
                        p = json.load(f)['stimulus_center']
                    position = [p['x'], p['y'], p['z']]
                    positions.append(position)
                positions = np.array(positions)
                
                data['positions'] = positions
                data['position_ids'] = position_ids
                
            
        return data

    def fetchProcessedData(self, *args):
        return self.loadNumpy(*args)

    def processData(self, *args, start=0, end=-1, name="", **data):
        super().processData()
        
        for key in args:
            if key=='ecg':
                if 'positions' in args:
                    inds = []
                    for position in data['position_ids']:
                        ind = np.where(position==np.array(data['ecg_ids']))[0]
                        #print(ind)
                        if len(ind)>0:
                            inds.append(ind[0])
                    data['ecg'] = data['ecg'][inds]
                    #print(inds)
                        
                        
                skip_inds = 600
                length = 200
                samples_per_label = 16
                
                X = np.array([])
                y = np.array([])
                
                for label, signal in enumerate(data['ecg'][start:end]):
                    print(label)
                    inds = np.random.randint(skip_inds, len(signal[:,0])-length, samples_per_label)
                        
                    _X = np.array([signal[ind:ind+length,:].T for ind in inds])
                        
                    if 'positions' in args:
                        _y = np.zeros((len(_X),3), dtype=int) + data['positions'][label]
                    else:
                        _y = np.zeros(len(_X), dtype=int) + label
                        
                    if len(X)==0:
                        X = _X
                    else:
                        X = np.append(X, _X, axis=0)
                            
                    if len(y)==0:
                        y = _y
                    else:
                        y = np.append(y, _y, axis=0)
                
                data[f'X_{name}'] = np.swapaxes(X, 1,2)
                data[f'y_{name}'] = y
                    
                #self.saveNumpy(**{f'X_{name}':data[f'X_{name}'], f'y_{name}':data[f'y_{name}']})
                    
        return data
    
    def saveNumpy(self, *args, **kwargs):
        super().saveNumpy(*args, **kwargs)

    def loadNumpy(self, *args):
        return super().loadNumpy(*args)

    def saveModel(self, model, **kwargs):
        super().saveModel(model, **kwargs)


if __name__ == '__main__': 
    mode = 0
    pipe = ModelPipeline()
    
    if mode == 0:
        """
        process from beginning
        """
        data = pipe.fetchRawData('ecg', 'positions')
        #d = data['positions']
        #fig = plt.figure()
        #ax = fig.add_subplot(111, projection='3d')
        
        #ax.scatter(d[:,0],d[:,1],d[:,2], c='r', marker='o')

        data = pipe.processData('ecg', 'positions', start=0, end=512, name='2', **data)
        pipe.saveNumpy(**data)
        del data['X_2']
        del data['y_2']
        
        data = pipe.processData('ecg', 'positions', start=0, end=512, name='3', **data)
        pipe.saveNumpy(**data)
        
        del data['X_3']
        del data['y_3']
        
        #data = pipe.processData('ecg', 'positions', start=256, end=-1, name=1, **data)
        #pipe.saveNumpy(**data)
        #del data['X_1']
        #del data['y_1']
        
        #pipe.saveNumpy(**data)
        
    elif mode == 1:
        """
        explore
        """
        data = pipe.fetchProcessedData('X', 'y', 'times', 'time_offset')
