from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Conv2D, Conv3D, Conv1D
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import Flatten, Dropout
from tensorflow.keras.layers import Concatenate, Permute, BatchNormalization
from tensorflow.keras.layers import MaxPool1D, GlobalAveragePooling1D
from tensorflow.keras import layers

import numpy as np
from sklearn.metrics import mean_absolute_error
from sklearn.base import BaseEstimator

import sys
modelFolder = '../../src/modeling/'
sys.path.append(modelFolder)


class AlwaysZeroClassifier(BaseEstimator):
    def fit(self, X, y=None):
        
        if y is not None:
            self.output_dim = np.shape(y)[1]
            
        return self
    
    def predict(self, X):
        return np.zeros(shape=(len(X), self.output_dim))
    
    def score(self, X, y=None):
        return mean_absolute_error(y, self.predict(X))
    
class RandomClassifier(BaseEstimator):
    def fit(self, X, y=None):
        
        if y is not None:
            self.output_dim = np.shape(y)[1]
            self.choice = np.array(list(set(y[:,0])))
        return self
    
    def predict(self, X):
        return np.random.choice(self.choice, len(X))
    
    def score(self, X, y=None):
        return mean_absolute_error(y, self.predict(X))
   

def CNN_clf(feature_dim, output_dim):      
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = Conv1D(32, 3, strides=1, activation='relu')(signal_input)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    
    for i in range(3):
        output = Conv1D(32, 3, strides=1)(output)
        output = BatchNormalization(momentum=0.01)(output)
        output = Activation('relu')(output)
        output = MaxPool1D(2)(output)
        output = Dropout(0.25)(output)
    
    output = Flatten()(output)                
    output = Dense(64, activation='relu')(output)
    output = Dense(output_dim, activation='softmax')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
    return model

def CNN_reg(feature_dim, output_dim=3):
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = Conv1D(32, 3, strides=1, activation='relu')(signal_input)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    
    for i in range(3):
        output = Conv1D(32, 3, strides=1)(output)
        output = BatchNormalization(momentum=0.01)(output)
        output = Activation('relu')(output)
        output = MaxPool1D(2)(output)
        output = Dropout(0.25)(output)
        
    for i in range(0):
        output = Conv1D(32, 3, strides=1)(output)
        output = BatchNormalization(momentum=0.01)(output)
        output = Activation('relu')(output)
        #output = MaxPool1D(2)(output)
        output = Dropout(0.25)(output)
    
    output = Flatten()(output)                
    output = Dense(64, activation='relu')(output)
    output = Dense(output_dim, activation='linear')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse','mae'])
        
    return model

def CNN_reg_big(feature_dim, output_dim=3):
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = Conv1D(128, 3, strides=1, activation='relu')(signal_input)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    
    output = Conv1D(128, 3, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)
    
    output = Conv1D(128, 4, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)

    output = Conv1D(128, 5, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)
    
    output = Conv1D(128, 4, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = Dropout(0.25)(output)
    
    output = Flatten()(output)                
    output = Dense(128, activation='relu')(output)
    output = Dense(output_dim, activation='linear')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse','mae'])
        
    return model

def CNN_embed_reg(feature_dim, output_dim=3):
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = Conv1D(128, 3, strides=1, activation='relu')(signal_input)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    
    output = Conv1D(128, 3, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)
    
    output = Conv1D(128, 3, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)
    
    output = Conv1D(64, 3, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = MaxPool1D(2)(output)
    output = Dropout(0.25)(output)
        
    output = Conv1D(32, 3, strides=1)(output)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    output = Dropout(0.25)(output)
    
    output = layers.GlobalAveragePooling1D()(output)              
    output = Dense(64, activation='relu')(output)
    output = Dense(output_dim, activation='linear')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse','mae'])
        
    return model

def lstm_pooling_reg(feature_dim=(200, 70), output_dim=3):
    model_input = layers.Input(shape=feature_dim, name='Input')
    #output = layers.GlobalAveragePooling1D()(model_input)
    
    output = layers.Bidirectional(layers.LSTM(64, return_sequences=True))(model_input)

    output = layers.GlobalAveragePooling1D()(output)
    output = layers.Dense(32, activation='relu')(output)
    output = layers.Dense(output_dim, activation='linear')(output)

    model = Model(inputs=model_input, outputs=output)
    model.compile(loss='mean_absolute_error', optimizer='adam', metrics=['mse','mae'])

    return model

def CNN_reg2(feature_dim, output_dim=3):      
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = Conv1D(32, 3, strides=1, activation='relu')(signal_input)
    output = BatchNormalization(momentum=0.01)(output)
    output = Activation('relu')(output)
    
    for i in range(3):
        output = Conv1D(32, 3, strides=1)(output)
        output = BatchNormalization(momentum=0.01)(output)
        output = Activation('relu')(output)
        output = MaxPool1D(2)(output)
        output = Dropout(0.25)(output)
    
    output = Flatten()(output)                
    output = Dense(64, activation='relu')(output)
    output = Dense(output_dim, activation='linear')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse','mae'])
        
    return model


def CNN_LSTM(feature_dim, output_dim):
    """
    Combination of Time-Distributed-CNN layer as input for a LSTM  network,
    output concatates with pixel-position to a fully-conntected layer
            
    feature_dim = [time-embedding, row, col, instance=1]
            
    """
        
    signal_input = Input(shape=feature_dim, name='Input')
        
    output = Conv1D(32, 3, strides=1, activation='relu')(signal_input)
    output = Conv1D(32, 3, strides=1, activation='relu')(output)
    output = MaxPool1D(2)(output)
    output = Conv1D(32, 3, strides=1, activation='relu')(output)
    output = Conv1D(32, 3, strides=1, activation='relu')(output)
    output = MaxPool1D(2)(output)
    #output = Flatten()(output)
        
    output = LSTM(64, return_sequences=False)(output)
    output = Activation('relu')(output)
                
    output = Dense(64, activation='relu')(output)
    output = Dense(output_dim, activation='softmax')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
    return model
    

def MLP(feature_dim, output_dim):
    """
    Combination of Time-Distributed-CNN layer as input for a LSTM  network,
    output concatates with pixel-position to a fully-conntected layer        
    feature_dim = [time-embedding, row, col, instance=1]           
    """
        
    signal_input = Input(shape=feature_dim, name='Input')
        
    output = Flatten()(signal_input)
    output = Dense(64, activation='relu')(output)
    output = Dropout(0.05)(output)
    output = Dense(64, activation='relu')(output)
    #output = Dropout(0.25)(output)
    output = Dense(output_dim, activation='softmax')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
    return model

def LSTM_clf(feature_dim, output_dim):
    signal_input = Input(shape=feature_dim, name='Input')
    
    output = LSTM(128, return_sequences=False)(signal_input)
    output = Dense(128, activation='relu')(output)
    output = Dense(output_dim, activation='softmax')(output)

    model = Model(signal_input, outputs=output)
    
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        
    return model

#from ann_visualizer.visualize import ann_viz


if __name__ == '__main__':
    model = CNN_clf((50,64), 13)
    model.summary()
    #ann_viz(model, title="My first neural network")
    
    
    
    
    
    
    
    
    
    
