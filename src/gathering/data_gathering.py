import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
import h5py, glob

def _gatherH5Coords(path=None, file=None):
    coords = None
    try:
        with h5py.File(path + file, 'r') as f:
            coords = f['Mesh']['10']['mesh']['geometry'][::1]
    except:
        with h5py.File(file, 'r') as f:
            coords = f['Mesh']['10']['mesh']['geometry'][::1]       
        
    return coords

def _gatherH5Data(pathU, pathSignal, file):
    u = []
    with h5py.File(pathU + file, 'r') as f:
        for s in range(len(f['VisualisationVector'])):
            u.append(f['VisualisationVector'][str(s)][:,0])
    u = np.reshape(u, np.shape(u))
    
    signal_times_ = pathSignal + 'times.npy'
    signals_ = glob.glob(pathSignal + '*electrode*')

    signal_times = np.load(signal_times_)
    
    signals = []  
    for signal_ in signals_:
        signal = np.load(signal_)
        signals.append(signal)
        #print(signal_)

    #!!!!!!! klappt nur wenn die Zeiten stimmen !!!!!!!!
    X = u[np.isin(np.linspace(0,2500,1001), signal_times)]

    Y = np.array(signals).T
    
    return X, Y


def _gatherTDMSData(path, file):
    from nptdms import TdmsFile
    
    tdms_file = TdmsFile(path + file)

    data = {}
    groups = tdms_file.groups()
    for group in groups:
        data[group] = {}
        channels = tdms_file.group_channels(group)
        for channel in channels:
            data[group][channel.channel] = channel.data
    
    return data
    #return tdms_file
    

class DelayEmbedder(BaseEstimator, TransformerMixin):
    #Class Constructor 
    def __init__(self, time_steps=2, stride=1):
        self.time_steps = time_steps
        self.stride = stride
    
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y=None):
        X_td = np.array([np.reshape([X[j-i] for i in range(0, self.time_steps*self.stride + 1, self.stride)], -1) for j in range(len(X))])[self.time_steps*self.stride:]
        if y is not None:
            return X_td, y[self.time_steps*self.stride:]
        return X_td
    
    def fit_transform(self, X, y=None):
        
        return self.fit(X, y).transform(X, y)
    

class DataTimesSelector(BaseEstimator, TransformerMixin ):
    #Class Constructor 
    def __init__(self):
        pass
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y = None):
        return X

    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
    
    
class DataCoordsSelector(BaseEstimator, TransformerMixin):
    #Class Constructor 
    def __init__(self, coords, selection='all', points=None, selection_radius=None):
        
        # points = [index1, ... , indexN]
        # feature= 'surface', 'inner', 'points', 'all', 'default'
        self._coords = coords
        self._selection = selection
        self._points = points # indices of wanted points
        self._selection_radius = selection_radius
        
        # define sphere with radius 'selection_radius'
        #for point in self._points:
        if self._selection == 'all' or self._selection == 'default':
            self.selected_points = np.array(range(len(self._coords)))
        
        elif self._selection == 'points':
            if self._selection_radius == None:
                self.selected_points = self._points
                #return self #X[:,self._points]
            else:
                # get points whoch fit the condition: distance to choosen points small enough
                # weird code but it works
                selected_points = np.array([])
                for point in self._points:
                    # calculate distance of choosen point to every single one
                    dist = np.linalg.norm(self._coords[point]-self._coords, axis=1)
                    # choose only point with a distance lower than selection_radius
                    sp = np.where(dist<=self._selection_radius)[0]     
                    selected_points = np.append(selected_points, sp)
                self.selected_points = np.array(list(set(selected_points)), dtype=np.int)
        else:
            self.selected_points = []
    
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y = None):
        if y is not None:
            return X[:,self.selected_points], y
        return X[:,self.selected_points]

    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
    
    
class MultiDimScaler(BaseEstimator, TransformerMixin):
    """
    Class Constructor 
    """
    
    def __init__(self):
        pass
    
    #Return self nothing else to do here    
    def fit(self, X, y=None ):
        self.minX = np.amin(X)
        self.maxX = np.amax(X)
        self.minY= np.amin(y)
        self.maxY = np.amax(y)
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y = None ):
        if y is not None:
            return (X-self.minX)/(self.maxX-self.minX), (y-self.minY)/(self.maxY-self.minY)
        return (X-self.minX)/(self.maxX-self.minX)

    def inverse_transform(self, X, y = None):
        if y is not None:
            return X*(self.maxX - self.minX) + self.minX,  y*(self.maxY - self.minY) + self.minY
        return X*(self.maxX - self.minX) + self.minX
    
    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
    
    
    
    
    
"""
def fetchData(path, file, ids=[]):
    data = []
    with h5py.File(path + file, 'r') as f:
        for s in range(len(f['VisualisationVector'])):
            if len(ids)==0:
                data.append(f['VisualisationVector'][str(s)][:,0])   
            else:
                data.append(f['VisualisationVector'][str(s)][ids,0]) 
    data = np.reshape(data, np.shape(data))
    return data

def fetchSignal(path, file, ids=[]):
    signal = []
    with h5py.File(path + file, 'r') as f:
        for s in range(len(f['VisualisationVector'])):
            if len(ids)==0:
                signal.append(f['VisualisationVector'][str(s)][:,0])   
            else:
                signal.append(f['VisualisationVector'][str(s)][ids,0]) 
    signal = np.reshape(signal, np.shape(signal))
    return signal

def fetchU(path, file, ids=[]):
    u = []
    with h5py.File(path + file, 'r') as f:
        for s in range(len(f['VisualisationVector'])):
            if len(ids)==0:
                u.append(f['VisualisationVector'][str(s)][:,0])   
            else:
                u.append(f['VisualisationVector'][str(s)][ids,0]) 
    u = np.reshape(u, np.shape(u))
    return u
"""
    
