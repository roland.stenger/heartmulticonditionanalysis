import numpy as np
from sklearn.base import TransformerMixin, BaseEstimator
    
class MinMaxScaler(TransformerMixin, BaseEstimator):
    """
    Class for scaling multidimensional arrays between min- and max-value
    """
    def __init__(self, ignore_value=None):
        self.ignore_value = ignore_value
    
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        self.minX = np.amin(X)
        self.maxX = np.amax(X)
        self.minY= np.amin(y)
        self.maxY = np.amax(y)
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y=None):
        if y is not None:
            return (X-self.minX)/(self.maxX-self.minX), (y-self.minY)/(self.maxY-self.minY)
        return (X-self.minX)/(self.maxX-self.minX)

    def inverse_transform(self, X, y=None):
        if y is not None:
            return X*(self.maxX - self.minX) + self.minX,  y*(self.maxY - self.minY) + self.minY
        return X*(self.maxX - self.minX) + self.minX
    
    
class DelayCoordinatesEmbedder(TransformerMixin, BaseEstimator):
    """
    Class for append delay coordinates, [x_i_t] -> [x_i_t, x_i_t-1, x_i_t-2, ...]
    """
    def __init__(self, time_steps=2, stride=1, flatten=True):
        self.time_steps = time_steps
        self.stride = stride
        self.flatten = flatten
    
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y=None):
        if self.flatten:
            X_td = np.array([np.reshape([X[j-i] for i in range(0, self.time_steps*self.stride + 1, self.stride)], -1) for j in range(len(X))])[self.time_steps*self.stride:]
            if y is not None:
                return X_td, y[self.time_steps*self.stride:]
            return X_td
        else:
            X_td = np.array([[X[j-i] for i in range(0, self.time_steps*self.stride + 1, self.stride)] for j in range(len(X))])[self.time_steps*self.stride:]
            if y is not None:
                return X_td, y[self.time_steps*self.stride:]
            return X_td

    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)




class CoordsSelector(BaseEstimator, TransformerMixin):
    #Class Constructor 
    def __init__(self, coords, selection='all', points=None, selection_radius=None):
        
        # points = [index1, ... , indexN]
        # feature= 'surface', 'inner', 'points', 'all', 'default'
        self._coords = coords
        self._selection = selection
        self._points = points # indices of wanted points
        self._selection_radius = selection_radius
        
        # define sphere with radius 'selection_radius'
        #for point in self._points:
        if self._selection == 'all' or self._selection == 'default':
            self.selected_points = np.array(range(len(self._coords)))
        
        elif self._selection == 'points':
            if self._selection_radius == None:
                self.selected_points = self._points
                #return self #X[:,self._points]
            else:
                # get points which fit the condition: distance to choosen points small enough
                # weird code but it works
                selected_points = np.array([])
                for point in self._points:
                    # calculate distance of choosen point to every single one
                    dist = np.linalg.norm(self._coords[point]-self._coords, axis=1)
                    # choose only point with a distance lower than selection_radius
                    sp = np.where(dist<=self._selection_radius)[0]     
                    selected_points = np.append(selected_points, sp)
                self.selected_points = np.array(list(set(selected_points)), dtype=np.int)
        else:
            self.selected_points = []
    
    #Return self nothing else to do here    
    def fit(self, X, y=None):
        return self 
    
    #Method that describes what we need this transformer to do
    def transform(self, X, y = None):
        if y is not None:
            return X[:,self.selected_points], y
        return X[:,self.selected_points]

    def fit_transform(self, X, y=None):
        return self.fit(X, y).transform(X, y)
