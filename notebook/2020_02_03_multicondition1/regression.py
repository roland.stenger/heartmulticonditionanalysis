
# coding: utf-8

# In[1]:


projectName = '2020_02_03_multicondition1/'

mainFolder = '../../pipeline/' + projectName + '/'
modelFolder = '../../src/modeling/'

import matplotlib.pyplot as plt # plotting
plt.style.use('ggplot')

import numpy as np # linear algebra
import pandas as pd

# make objects from folders below importable
import sys
sys.path.append(mainFolder) # contains 'pipeline' for fetching, processing and saving data, models etc...
from pipeline import ModelPipeline

sys.path.append(modelFolder) # contains 'models' like CNN, LTSM, MLP etc...
#from models import CNN_clf, CNN_clf_small, CNN_clf_tiny, CNN_clf_huge

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from classifier import CNN_reg, lstm_pooling_reg
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint


# In[2]:
def getDataByName(name=0):

    pipeline = ModelPipeline()
    X = pipeline.fetchProcessedData(f'X_{name}')[f'X_{name}']
    y = pipeline.fetchProcessedData(f'y_{name}')[f'y_{name}']
    
    for i in range(len(X[0,0,:])):
        scaler = MinMaxScaler()
        X[:,:,i] = scaler.fit_transform(X[:,:,i])

    X_train, X_test, y_train, y_test =  train_test_split(X, y, test_size=0.20, shuffle=True)
    del X, y
    return X_train, X_test, y_train, y_test


# In[2]:
def getData():

    pipeline = ModelPipeline()
    X = pipeline.fetchProcessedData('X_')['X_']
    y = pipeline.fetchProcessedData('y_')['y_']
    
    for i in range(len(X[0,0,:])):
        scaler = MinMaxScaler()
        X[:,:,i] = scaler.fit_transform(X[:,:,i])

    X_train, X_test, y_train, y_test =  train_test_split(X, y, test_size=0.20, shuffle=True)
    del X, y
    return X_train, X_test, y_train, y_test
# In[6]:

model = CNN_reg(feature_dim=(300,70), output_dim=3) #(50 delay-steps, 70 channel)
print(model.summary())

# In[ ]:

es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=16)
mc = ModelCheckpoint('best_model.h5', monitor='val_loss', mode='min', save_best_only=True)

for i in [0]:
    X_train, X_test, y_train, y_test = getData()
    model.fit(X_train, y_train, 
            epochs=256, 
            batch_size=128, 
            verbose=1, 
            callbacks=[es, mc],
            validation_data=(X_test, y_test))
    del X_train, X_test, y_train, y_test
# In[10]:

#plt.plot(model.history.history['loss'])


# In[11]:

#model.predict(X_test[:16])


# In[12]:

#y_test[:16]

